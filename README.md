# Foundry VTT - Bullseye

![Bullseye Preview](https://gitlab.com/Ionshard/foundry-vtt-bullseye/-/wikis/uploads/91fba54fd59978b1d2a9c4bd85c7a895/fvtt-bullseye.png)

## Install

### Foundry Add-On Module Installation

- Go to Foundry VTT's Configuration and Setup Menu
- Select the Add-on Modules tab
- Select the Install Module button
- Use the link [https://gitlab.com/Ionshard/foundry-vtt-bullseye/-/jobs/artifacts/master/raw/module.json?job=build-module](https://gitlab.com/Ionshard/foundry-vtt-bullseye/-/jobs/artifacts/master/raw/module.json?job=build-module) as the Manifest URL
- Select Install

## Usage

Right click the Anvil in the top right and select `Open Bullseye`.

## Anvil Menu

This application uses the [Anvil Menu](https://gitlab.com/Ionshard/foundry-vtt-anvil-menu) library to register itself on an extendable context menu on the Foundry Anvil.

### Manual Install

Download the [bullseye.zip](https://gitlab.com/Ionshard/foundry-vtt-bullseye/-/jobs/artifacts/master/raw/bullseye.zip?job=build-module) module and extract it to the `data/modules` directory inside Foundry VTT's user data directory.

## License
<a rel="license" href="https://spdx.org/licenses/MIT.html"><img alt="MIT License" style="border-width:0" src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/License_icon-mit-88x31-2.svg/88px-License_icon-mit-88x31-2.svg.png" /></a> Bullseye - a module for Foundry VTT - by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/Ionshard/foundry-vtt-bullseye" property="cc:attributionName" rel="cc:attributionURL">Victor Ling</a> is licensed under an <a rel="license" href="https://spdx.org/licenses/MIT.html"> MIT License</a>.

This work is licensed under the Foundry Virtual Tabletop [Limited License Agreement for Module Development](https://foundryvtt.com/article/license/).


