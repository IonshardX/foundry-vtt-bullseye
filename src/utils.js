export const i18n = (key) => game.i18n && game.i18n.localize(key)
